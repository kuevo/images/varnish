#!/bin/sh

set -e

if [ -n "$VARNISH_PURGE_HOSTS" ]; then
    # Replace ${VARNISH_PURGE_HOSTS} in default.vcl with the generated lines
    LINES="\"localhost\";\n"
    for host in $(echo $VARNISH_PURGE_HOSTS | tr "," "\n"); do
        LINES="$LINES    \"$host\";\n"
    done
    sed -i "s|\${VARNISH_PURGE_HOSTS}|$LINES|g" /etc/varnish/$PROJECTTYPE.vcl.tmpl
fi

# Create the varnish.vcl
envsubst "$(env | sed -e 's/=.*//' -e 's/^/$/g')" < /etc/varnish/$PROJECTTYPE.vcl.tmpl > /etc/varnish/default.vcl

set -e

# this will check if the first argument is a flag
# but only works if all arguments require a hyphenated flag
# -v; -SL; -f arg; etc will work, but not arg1 arg2
if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
    varnishd \
    -F \
    -f /etc/varnish/default.vcl \
    -a http=:80,HTTP \
    -s malloc,$VARNISH_SIZE \
    -p workspace_backend=128k \
    -p http_resp_hdr_len=128k \
    -p http_resp_size=256k &

    /usr/bin/varnishncsa -F '%{Host}i %h %l %u %t \"%r\" %s %b \"%{Referer}i\" \"%{User-agent}i\" \"%{Varnish:hitmiss}x\"'
fi

exec "$@"
