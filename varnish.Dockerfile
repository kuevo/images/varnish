# syntax=docker/dockerfile:1.4
FROM bitnami/minideb:bullseye

# Run as root
USER root

# Non interactive
ENV DEBIAN_FRONTEND=noninteractive

# Install the various packages
ARG VARNISH_VERSION="72"
ARG VMOD_DYNAMIC_VERSION="7.2"
RUN \
    apt update && \
    apt upgrade -y && \
    install_packages \
        software-properties-common \
        gettext-base \
        gnupg \
        wget \
        curl \
        git \
        build-essential \
        autoconf \
        automake \
        libtool \
        libgetdns10 \
        libgetdns-dev \
        rst2pdf \
    && \
    wget -qO- https://packagecloud.io/install/repositories/varnishcache/varnish${VARNISH_VERSION}/script.deb.sh | bash - && \
    install_packages \
        varnish \
        varnish-dev \
    && \
    # Build libvmod-dynamic for varnish, so that dynamic dns works
    git clone https://github.com/nigoroll/libvmod-dynamic.git /tmp/libvmod-dynamic && \
    cd /tmp/libvmod-dynamic && \
    git checkout ${VMOD_DYNAMIC_VERSION} && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install && \
    cd /tmp && \
    rm -rf /tmp/libvmod-dynamic && \
    # Cleanup
    apt remove -y \
        software-properties-common \
        gnupg \
        wget \
        curl \
        git \
        autoconf \
        automake \
        libtool \
        libgetdns-dev \
        build-essential \
    && \
    apt clean && \
    apt autoclean -y && \
    apt autoremove -y && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY --link config/entrypoint.sh /entrypoint.sh
COPY --link templates/* /etc/varnish/

RUN chmod +x /entrypoint.sh

ARG VARNISH_UPSTREAM_HOSTNAME="127.0.0.1"
ENV VARNISH_UPSTREAM_HOSTNAME=${VARNISH_UPSTREAM_HOSTNAME}
ARG VARNISH_PURGE_HOSTS="127.0.0.1"
ENV VARNISH_PURGE_HOSTS=${VARNISH_PURGE_HOSTS}
ARG VARNISH_NAT_GATEWAY="127.0.0.1"
ENV VARNISH_NAT_GATEWAY=${VARNISH_NAT_GATEWAY}
ENV VARNISH_SIZE="256m"
ENV PROJECTTYPE="magento"

USER root
EXPOSE 80
ENTRYPOINT ["/entrypoint.sh"]
